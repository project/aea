# Advanced Entity Autocomplete

This module adds an entity autocomplete form widget that replaces the field
with an entity view display once an entity was chosen from the autocomplete.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/aea).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/aea).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module does not have configuration.


## Maintainers

- [Randal Vanheede (RandalV)](https://www.drupal.org/u/randalv)
