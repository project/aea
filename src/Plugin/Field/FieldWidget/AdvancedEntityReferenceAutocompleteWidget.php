<?php

namespace Drupal\aea\Plugin\Field\FieldWidget;

use Drupal\aea\Controller\ClearTempStoreController;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'advanced_entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "advanced_entity_reference_autocomplete",
 *   label = @Translation("Advanced Autocomplete"),
 *   description = @Translation("A more advanced autocomplete text field."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class AdvancedEntityReferenceAutocompleteWidget extends EntityReferenceAutocompleteWidget {

  const DEFAULT_DISPLAY_MODE = 'full';
  const DEFAULT_BUTTON_LABEL = 'Choose entity';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $privateTempStore;

  /**
   * Constructs the AdvancedEntityReferenceAutocomplete widget class.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings, array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    PrivateTempStoreFactory $private_temp_store_factory
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->privateTempStore = $private_temp_store_factory->get(ClearTempStoreController::PRIVATE_TEMPSTORE_STORAGE_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'display_mode' => self::DEFAULT_DISPLAY_MODE,
        'button_label' => self::DEFAULT_BUTTON_LABEL,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    // Prefill the display mode options.
    $display_options = ['default' => 'Default'];
    $field_settings = $this->getFieldSettings();
    $target_type = $field_settings['target_type'] ?? 'node';
    if (isset($field_settings['handler_settings']['target_bundles'])) {
      foreach ($field_settings['handler_settings']['target_bundles'] as $bundle) {
        foreach ($this->entityDisplayRepository->getViewModeOptionsByBundle($target_type, $bundle) as $display_mode => $label) {
          $display_options[$display_mode] = $label;
        }
      }
    }
    else {
      foreach ($this->entityDisplayRepository->getViewModeOptions($target_type) as $display_mode => $options) {
        $display_options[$display_mode] = $options['label'] ?? ucfirst($display_mode);
      }
    }

    $element['display_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Display mode'),
      '#description' => $this->t('The display mode to render instead of the autocomplete textfield.'),
      '#default_value' => $this->getSetting('display_mode', self::DEFAULT_DISPLAY_MODE),
      '#options' => $display_options,
    ];

    $element['button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button label'),
      '#description' => $this->t('The label for the change entity-button.'),
      '#default_value' => $this->getSetting('button_label', self::DEFAULT_BUTTON_LABEL),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Display mode: @display_mode', [
      '@display_mode' => $this->getSetting('display_mode', self::DEFAULT_DISPLAY_MODE),
    ]);
    $summary[] = $this->t('Button label: @button_label', [
      '@button_label' => $this->getSetting('button_label', self::DEFAULT_BUTTON_LABEL),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $field_name = $this->fieldDefinition->getName();
    $field_settings = $this->getFieldSettings();
    $referenced_entities = $items->referencedEntities();
    $triggering_element = $form_state->getTriggeringElement();
    $tempstore = unserialize($this->privateTempStore->get($this->getTempStoreKey())) ?: [];
    $cleaned = FALSE;
    $entity = NULL;

    // Reset the tempstore when we want to change the chosen entity.
    if (isset($triggering_element['#array_parents']) && isset($triggering_element['#element_delta'])
      && $triggering_element['#element_delta'] == $delta
      && ($triggering_element_parent = array_pop($triggering_element['#array_parents']))) {
      if ($triggering_element_parent === 'aea_change_entity') {
        self::cleanTempStore($this->getTempStoreKey(), $delta);
        $tempstore = unserialize($this->privateTempStore->get($this->getTempStoreKey())) ?: [];
        $cleaned = TRUE;
      }
    }

    // Otherwise check if we've processed the entity in our ajax call.
    if (isset($user_input[$field_name][$delta]['target_id'])) {
      if ($entity_id = $this->transformReferenceId($user_input[$field_name][$delta]['target_id'])) {
        $entity = $this->entityTypeManager
          ->getStorage($field_settings['target_type'])
          ->load($entity_id);
      }
    }
    // Otherwise check if entity is stored in our tempstore.
    elseif ($entity_id = $this->transformReferenceId($tempstore[$delta] ?? NULL)) {
      $entity = $this->entityTypeManager
        ->getStorage($field_settings['target_type'])
        ->load($entity_id);
    }
    // Otherwise, check if there's a default value.
    elseif (!$cleaned && isset($referenced_entities[$delta])
      && $referenced_entities[$delta] instanceof EntityInterface) {
      $entity = $referenced_entities[$delta];
    }

    // Replace the entity reference form element by a
    // container that contains the element (along with a button).
    $element = [
      '#type' => 'container',
      '#prefix' => '<div class="aea-entity-information--' . $field_name . '-' . $delta . '">',
      '#suffix' => '</div>',
      'aea_change_entity' => [
        // We have to change the '#type' since setting '#access' breaks
        // the ajax functionality.
        '#type' => $entity instanceof EntityInterface ? 'button' : 'hidden',
        '#name' => 'aea_change_entity__' . $field_name . '_' . $delta,
        '#value' => $this->t($this->getSetting('button_label', self::DEFAULT_BUTTON_LABEL)),
        '#limit_validation_errors' => [],
        '#element_delta' => $delta,
        '#weight' => -1,
        '#ajax' => [
          'callback' => [$this, 'processEntity'],
          'event' => 'click',
        ],
      ],
    ] + parent::formElement($items, $delta, $element, $form, $form_state);
    $element['target_id']['#weight'] = -2;
    $element['target_id']['#element_delta'] = $delta;
    $element['target_id']['#ajax'] = [
      'callback' => [$this, 'processEntity'],
      'event' => 'autocompleteclose',
      'progress' => FALSE,
    ];

    // If an entity is found, load the entity's configured display mode.
    if ($entity instanceof EntityInterface) {
      if ($this->getSetting('display_mode') !== self::DEFAULT_DISPLAY_MODE) {
        $element['entity'] = $this->entityTypeManager
          ->getViewBuilder($field_settings['target_type'])
          ->view($entity, $this->getSetting('display_mode'));
      }
      else {
        $element['entity'] = $this->entityTypeManager
          ->getViewBuilder($field_settings['target_type'])
          ->view($entity);
      }

      // Set the related entity field.
      $element['target_id']['#default_value'] = $entity;

      // Set the tempstore value.
      $tempstore[$delta] = $entity->id();
      $this->privateTempStore->set($this->getTempStoreKey(), serialize($tempstore));

      // Hide the entity field when entity is loaded.
      $element['target_id']['#access'] = FALSE;
    }
    else {
      // Set the related entity field.
      $element['target_id']['#default_value'] = NULL;
    }

    $form['#attached']['drupalSettings']['aea']['tempStoreKey'] = $this->getTempStoreKey();
    $form['#attached']['drupalSettings']['aea']['clearRoute'] = Url::fromRoute('aea.clear_tempstore')->toString();
    if (!isset($form['#attached']['library'])
      || !in_array('aea/reset_tempstore', $form['#attached']['library'])) {
      $form['#attached']['library'][] = 'aea/reset_tempstore';
    }

    return $element;
  }

  /**
   * Loads the entity.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function processEntity(array &$form, FormStateInterface $form_state): AjaxResponse {
    $field_name = $this->fieldDefinition->getName();
    $triggering_element = $form_state->getTriggeringElement();
    $delta = $triggering_element['#element_delta'];
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand(
      'div.aea-entity-information--' . $field_name . '-' . $delta,
      $form[$field_name]['widget'][$delta]
    ));
    return $response;
  }

  /**
   * Returns the tempstore storage key.
   *
   * @return string
   *   The tempstore key.
   */
  protected function getTempStoreKey(): string {
    return $this->fieldDefinition->getName() . ':' . $this->fieldDefinition->getUniqueIdentifier();
  }

  /**
   * Transforms an entity reference value into an entity ID.
   *
   * @param string|null $value
   *   The entity reference value.
   *
   * @return ?string
   *   The ID.
   */
  protected function transformReferenceId(?string $value): ?string {
    if (empty($value)) {
      return NULL;
    }
    elseif (is_numeric($value)) {
      return $value;
    }

    // Get the matches for this.
    preg_match('/\([0-9]+\)/', $value, $matches);

    // Retrieve the last match, since in our case this will always be the ID.
    // This is a fallback for edge cases like: 'Firstname Lastname (3) (34)'
    // where '34' is the ID and '(39)' is just part of the entity's name.
    $match = array_pop($matches);

    // Trim the brackets from the result.
    return trim($match, '()');
  }

  /**
   * Cleans the tempstore values.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public static function cleanTempStore(string $tempstore_key, $delta = NULL) {
    /**
     * @var \Drupal\Core\TempStore\PrivateTempStore $tempstore
     */
    $tempstore = \Drupal::service('tempstore.private')
        ->get(ClearTempStoreController::PRIVATE_TEMPSTORE_STORAGE_KEY);

    if (!empty($delta)) {
      $data = unserialize($tempstore->get($tempstore_key)) ?? [];
      unset($data[$delta]);
      $tempstore->set($tempstore_key, serialize($data));
    }
    else {
      // Remove the entity id from tempstore.
      $tempstore->delete($tempstore_key);
    }
  }

  /**
   * Override of the default method, allows for a default value.
   *
   * @param string $key
   *   The setting key.
   * @param mixed $default
   *   The default value.
   *
   * @return mixed|null
   *   The requested setting.
   */
  public function getSetting($key, $default = NULL) {
    return !empty(parent::getSetting($key)) ? parent::getSetting($key) : $default;
  }

}
