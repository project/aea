<?php

namespace Drupal\aea\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a controller to clear private tempstore values.
 */
class ClearTempStoreController extends ControllerBase {

  /**
   * Defines the storage key for the private tempstore.
   */
  const PRIVATE_TEMPSTORE_STORAGE_KEY = 'advanced_entity_autocomplete_storage';

  /**
   * The private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $privateTempStore;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected Request $request;

  /**
   * Constructs the controller.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private tempstore factory.
   */
  public function __construct(PrivateTempStoreFactory $private_temp_store_factory, RequestStack $request_stack) {
    $this->privateTempStore = $private_temp_store_factory->get(self::PRIVATE_TEMPSTORE_STORAGE_KEY);
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('request_stack')
    );
  }

  /**
   * Clears the tempstore by a supplied key.
   *
   * @param string $key
   *   The tempstore key
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The json response.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function clear(): JsonResponse {
    if (!$this->request->request->has('key')) {
      return new JsonResponse([
        'message' => $this->t('Could not clear tempstore since no key was provided in post data.')
      ], 400);
    }

    $key = $this->request->request->get('key');
    $this->privateTempStore->delete($key);
    return new JsonResponse([
      'message' => $this->t('The AEA tempstore for key @key was cleared.', [
        '@key' => $key,
      ])
    ], 200);
  }

}
