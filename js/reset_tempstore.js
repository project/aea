(function ($, Drupal) {
    Drupal.behaviors.aeaBehavior = {
        attach: function (context, settings) {
            let tempStoreKey = drupalSettings.aea.tempStoreKey;
            let routeUrl = drupalSettings.aea.clearRoute;
            window.addEventListener('beforeunload', (event) => {
                $.post(routeUrl, { key: tempStoreKey });
                event.stopImmediatePropagation(); // Prevents more than one submission.
                return false;
            });
        }
    };
})(jQuery, Drupal);
